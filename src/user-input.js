const readline = require('readline-promise').default;

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

async function readArray(prompt) {
	const input = await rl.questionAsync(prompt);
	try {
		return JSON.parse(input);
	}
	catch (error)
	{
		console.log('That was not an array!');
		rl.close();
	}
}

async function readInteger(prompt) {
	const input = await rl.questionAsync(prompt);
	if (isPositiveInteger(input)) {
		return parseInt(input);
	}
	else {
		console.log('That was not a positive integer!');
		rl.close();
	}
}

function close() {
	rl.close();
}

function isPositiveInteger(str) {
  return /^\+?[1-9][\d]*$/.test(str);
}

module.exports = { readArray, readInteger, close };
