const UserInput = require('./user-input');

function groupArrayElements(array, numGroups) {
    const groupSize = Math.ceil(array.length / numGroups);

    const groups = [];
    for (let i = 0; i < numGroups; i++) {
        groups.push(array.splice(0, groupSize));
    }
    return groups;
}

(async () => {
    const array = await UserInput.readArray('Enter array to split into groups:');
    if (!array) {
		return;
	}

    const numGroups = await UserInput.readInteger('Enter desired number of groups:');
    if (!numGroups) {
		return;
	}

    UserInput.close();
    const result = groupArrayElements(array, numGroups);
	console.log(`Result: ${JSON.stringify(result)}`);
})();
