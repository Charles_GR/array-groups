1. Ensure that NodeJS is installed.
2. Open a terminal window here.
3. Type 'npm install' and press enter.
4. Type 'npm start' and press enter.
5. Enter array to split into groups, like [1,2,3].
6. Enter desired number of groups, like 2.
7. See result of splitting array into groups.